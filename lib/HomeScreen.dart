import 'package:flutter/material.dart';
import 'package:manvia_app/HouseDetailScreen.dart';
import 'package:manvia_app/fab_bottom_app_bar.dart';
import 'package:manvia_app/general_model.dart';
import 'package:manvia_app/wavy_header.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:page_view_indicator/page_view_indicator.dart';

class HomeScreen extends StatefulWidget {

  @override
  _HomeScreenState createState() => _HomeScreenState();

}

class _HomeScreenState extends State<HomeScreen> {

  String _lastSelected = 'TAB: 0';

  double rating = 3;

  int currentPosition = 1;

  PageController _pageController = new PageController(initialPage: 0);

  final pageIndexNotifier = ValueNotifier(0);

  List<String> photos = [
    "assets/images/house1.jpg",
    "assets/images/house2.jpeg",
  ];

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      body: new SingleChildScrollView(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            new Stack(

              children: <Widget>[
                WavyHeader(),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(top:100.0),
                    child: Container(
                      height: 50.0,

                      margin: const EdgeInsets.all(10.0),
                      padding: const EdgeInsets.symmetric(horizontal: 20.0,),
                      decoration: new BoxDecoration(
                        color: Color(0xFFFCFCFC).withOpacity(0.3),
                        borderRadius: new BorderRadius.circular(10.0),
                      ),
                      child: Row(
                        children: <Widget>[
                          Expanded(child:Text('Essayez "Abidjan',style: TextStyle(color: Color(0xFFFCFCFC)),)),
                          IconButton(icon:Icon(Icons.search), onPressed: (){

                          },color: Color(0xFFFCFCFC),iconSize: 30.0,)
                        ],
                      ) ,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top:40.0,left: 10.0,),
                  child: Align(
                      alignment: Alignment.topLeft,
                      //child: Image.asset('assets/images/name.png',height: 40.0,color: Colors.white,)),
                   child: Text("Manvia",style: TextStyle(fontSize: 40.0,color: Color(0xFFFCFCFC),fontWeight: FontWeight.bold),)),
                )



              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top:10.0,left: 10.0,),
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Text("Catégories",style: TextStyle(fontSize: 28.0,color: Colors.black,fontWeight: FontWeight.bold),)),
            ),
            Container(
              padding: EdgeInsets.only(left: 10.0),
              height: 150.0,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    return _buildFruitCategoryList(context,index,fruitsCategoryList.fruitsCategory);
                  }),
            ),

            Padding(
              padding: const EdgeInsets.only(top:10.0,left: 10.0, bottom: 10.0),
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Text("90 Logements",style: TextStyle(fontSize: 28.0,color: Colors.black,fontWeight: FontWeight.bold),)),
            ),
            new Container(
              height: MediaQuery.of(context).size.height - 500.0,
              width: MediaQuery.of(context).size.width,
              child: new ListView(
                children: <Widget>[
                  new Container(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                    child: new InkWell(
                      onTap: (){
                        Navigator.push(context, new MaterialPageRoute(builder: (context)=>HouseDetailScreen()));
                      },
                      child: new Column(
                        children: <Widget>[
                          new Container(
                            width: MediaQuery.of(context).size.width - 40.0,
                            height: 220.0,
                            child: new Stack(children: <Widget>[

                              new Container(
                                width: double.infinity,
                                height: 220.0,
                                child: PageView(
                                  controller: _pageController,
                                  onPageChanged: (int index){
                                    setState(() {

                                      currentPosition  = index + 1;
                                    });
                                  },
                                  children: photos.map((photo){
                                    return ClipRRect(
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(5.0),  topRight:Radius.circular(5.0)),
                                        child: Image.asset(
                                          photo,
                                          width: MediaQuery.of(context).size.width - 40.0,
                                          height: 220.0,

                                          fit: BoxFit.cover,));
                                  }).toList(),
                                ),
                              )

                              ,
                              new Container(
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topRight,
                                      child: new IconButton(icon: Icon(Icons.favorite_border, color: Colors.white,), onPressed: (){

                                      }),
                                    ),
                                    new Container(
                                      padding: EdgeInsets.all(10.0),
                                      color: Colors.transparent,
                                      child: PageViewIndicator(
                                        pageIndexNotifier: pageIndexNotifier,
                                        length: photos.length,
                                        normalBuilder: (animationController, index) => Circle(
                                          size: 9.0,
                                          color: Colors.grey[200],
                                        ),
                                        highlightedBuilder: (animationController, index) => ScaleTransition(
                                          scale: CurvedAnimation(
                                            parent: animationController,
                                            curve: Curves.ease,
                                          ),
                                          child: Circle(
                                            size: 11.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )

                            ],),
                          ),

                          new Container(
                            child: new Column(
                              children: <Widget>[
                                new SizedBox(height:10.0),
                                new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Text('RESIDENCES MEUBLEES', style: new TextStyle(color: Colors.blue[900], fontWeight: FontWeight.bold),),
                                ),
                                new SizedBox(height:10.0),
                                new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Text('RESIDENCES ABBA A Angré', style: new TextStyle(color: Colors.black87, fontSize: 20.0,fontWeight: FontWeight.bold),),
                                ),
                                new SizedBox(height:3.0),
                                new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Text('FCFA 15 000 / Nuit', style: new TextStyle(color: Colors.black, fontSize: 16.0,),),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            alignment: Alignment.topLeft,
                            child:  SmoothStarRating(
                                allowHalfRating: false,
                                onRatingChanged: (v) {
                                  //rating = v;
                                  setState(() {});
                                },
                                starCount: 5,
                                rating: rating,
                                size: 18.0,
                                color: Colors.red,
                                borderColor: Colors.red,
                                spacing:0.0
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  new Container(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                    child: new InkWell(
                      onTap: (){},
                      child: new Column(
                        children: <Widget>[
                          new Container(
                            width: MediaQuery.of(context).size.width - 40.0,
                            height: 220.0,
                            child: new Stack(children: <Widget>[

                              new Container(
                                width: double.infinity,
                                height: 220.0,
                                child: PageView(
                                  controller: _pageController,
                                  onPageChanged: (int index){
                                    setState(() {

                                      currentPosition  = index + 1;
                                    });
                                  },
                                  children: photos.map((photo){
                                    return ClipRRect(
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(5.0),  topRight:Radius.circular(5.0)),
                                        child: Image.asset(
                                          photo,
                                          width: MediaQuery.of(context).size.width - 40.0,
                                          height: 220.0,

                                          fit: BoxFit.cover,));
                                  }).toList(),
                                ),
                              )

                              ,
                              new Container(
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topRight,
                                      child: new IconButton(icon: Icon(Icons.favorite_border, color: Colors.white,), onPressed: (){

                                      }),
                                    ),
                                    new Container(
                                      padding: EdgeInsets.all(10.0),
                                      color: Colors.transparent,
                                      child: PageViewIndicator(
                                        pageIndexNotifier: pageIndexNotifier,
                                        length: photos.length,
                                        normalBuilder: (animationController, index) => Circle(
                                          size: 9.0,
                                          color: Colors.grey[200],
                                        ),
                                        highlightedBuilder: (animationController, index) => ScaleTransition(
                                          scale: CurvedAnimation(
                                            parent: animationController,
                                            curve: Curves.ease,
                                          ),
                                          child: Circle(
                                            size: 11.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )

                            ],),
                          ),

                          new Container(
                            child: new Column(
                              children: <Widget>[
                                new SizedBox(height:10.0),
                                new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Text('RESIDENCES MEUBLEES', style: new TextStyle(color: Colors.blue[900], fontWeight: FontWeight.bold),),
                                ),
                                new SizedBox(height:10.0),
                                new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Text('RESIDENCES ABBA A Angré', style: new TextStyle(color: Colors.black87, fontSize: 20.0,fontWeight: FontWeight.bold),),
                                ),
                                new SizedBox(height:3.0),
                                new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Text('FCFA 15 000 / Nuit', style: new TextStyle(color: Colors.black, fontSize: 16.0,),),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            alignment: Alignment.topLeft,
                            child:  SmoothStarRating(
                                allowHalfRating: false,
                                onRatingChanged: (v) {
                                  //rating = v;
                                  setState(() {});
                                },
                                starCount: 5,
                                rating: rating,
                                size: 18.0,
                                color: Colors.red,
                                borderColor: Colors.red,
                                spacing:0.0
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  new Container(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
                    child: new InkWell(
                      onTap: (){},
                      child: new Column(
                        children: <Widget>[
                          new Container(
                            width: MediaQuery.of(context).size.width - 40.0,
                            height: 220.0,
                            child: new Stack(children: <Widget>[

                              new Container(
                                width: double.infinity,
                                height: 220.0,
                                child: PageView(
                                  controller: _pageController,
                                  onPageChanged: (int index){
                                    setState(() {

                                      currentPosition  = index + 1;
                                    });
                                  },
                                  children: photos.map((photo){
                                    return ClipRRect(
                                        borderRadius: BorderRadius.only(topLeft: Radius.circular(5.0),  topRight:Radius.circular(5.0)),
                                        child: Image.asset(
                                          photo,
                                          width: MediaQuery.of(context).size.width - 40.0,
                                          height: 220.0,

                                          fit: BoxFit.cover,));
                                  }).toList(),
                                ),
                              )

                              ,
                              new Container(
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    new Container(
                                      alignment: Alignment.topRight,
                                      child: new IconButton(icon: Icon(Icons.favorite_border, color: Colors.white,), onPressed: (){

                                      }),
                                    ),
                                    new Container(
                                      padding: EdgeInsets.all(10.0),
                                      color: Colors.transparent,
                                      child: PageViewIndicator(
                                        pageIndexNotifier: pageIndexNotifier,
                                        length: photos.length,
                                        normalBuilder: (animationController, index) => Circle(
                                          size: 9.0,
                                          color: Colors.grey[200],
                                        ),
                                        highlightedBuilder: (animationController, index) => ScaleTransition(
                                          scale: CurvedAnimation(
                                            parent: animationController,
                                            curve: Curves.ease,
                                          ),
                                          child: Circle(
                                            size: 11.0,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )

                            ],),
                          ),

                          new Container(
                            child: new Column(
                              children: <Widget>[
                                new SizedBox(height:10.0),
                                new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Text('RESIDENCES MEUBLEES', style: new TextStyle(color: Colors.blue[900], fontWeight: FontWeight.bold),),
                                ),
                                new SizedBox(height:10.0),
                                new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Text('RESIDENCES ABBA A Angré', style: new TextStyle(color: Colors.black87, fontSize: 20.0,fontWeight: FontWeight.bold),),
                                ),
                                new SizedBox(height:3.0),
                                new Container(
                                  alignment: Alignment.topLeft,
                                  child: new Text('FCFA 15 000 / Nuit', style: new TextStyle(color: Colors.black, fontSize: 16.0,),),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            alignment: Alignment.topLeft,
                            child:  SmoothStarRating(
                                allowHalfRating: false,
                                onRatingChanged: (v) {
                                  //rating = v;
                                  setState(() {});
                                },
                                starCount: 5,
                                rating: rating,
                                size: 18.0,
                                color: Colors.red,
                                borderColor: Colors.red,
                                spacing:0.0
                            ),
                          )
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            )

          ],
        ),
      ),
      bottomNavigationBar: FABBottomAppBar(
        centerItemText: 'Publier',
        color: Colors.grey,
        selectedColor: Colors.red,
        notchedShape: CircularNotchedRectangle(),
        onTabSelected: _selectedTab,
        items: [
          FABBottomAppBarItem(iconData: Icons.home, text: 'Accueil'),
          FABBottomAppBarItem(iconData: Icons.favorite, text: 'Favoris'),
          FABBottomAppBarItem(iconData: Icons.navigation, text: 'Bookings'),
          FABBottomAppBarItem(iconData: Icons.account_circle, text: 'Profil'),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _buildFab(
          context),
    );

  }

  void _selectedTab(int index) {
    setState(() {
      _lastSelected = 'TAB: $index';
    });
  }

  void _selectedFab(int index) {
    setState(() {
      _lastSelected = 'FAB: $index';
    });
  }
  Widget _buildFab(BuildContext context) {
    final icons = [ Icons.sms, Icons.mail, Icons.phone ];
    return FloatingActionButton(
      onPressed: () {
       /* Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SecondScreen()),
        );*/
      },
      tooltip: 'manvia',
      backgroundColor: Colors.white,
      child: Icon(Icons.add, color: Colors.red,),
      elevation: 2.0,

    );
  }

  Widget _buildFruitCategoryList(context,index, List<FruitsCategory>listImages) {


    return
      Padding(
        padding: const EdgeInsets.only(left:10.0,top: 19.0),
        child:  Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey[300]),
            borderRadius: BorderRadius.circular(5.0)
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              ClipRRect(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5.0),  topRight:Radius.circular(5.0)),
                  child: Image.asset(

                    listImages[index].image,
                    width: 180.0,
                    height: 90.0,

                    fit: BoxFit.cover,)),
              new Container(
                padding: EdgeInsets.all(10.0),
                child: Text(listImages[index].name, style: new TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),),
              )
            ],
          ),
        ),






      );
  }

}
