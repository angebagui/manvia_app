import 'package:flutter/material.dart';
import 'package:manvia_app/FullScreenImageWidget.dart';
import 'package:page_view_indicator/page_view_indicator.dart';

class HouseDetailScreen extends StatefulWidget {

  @override
  _HouseDetailScreenState createState() => _HouseDetailScreenState();

}

class _HouseDetailScreenState extends State<HouseDetailScreen> {

  double rating = 3;

  int currentPosition = 1;

  PageController _pageController = new PageController(initialPage: 0);

  final pageIndexNotifier = ValueNotifier(0);

  List<String> photos = [
    "assets/images/house1.jpg",
    "assets/images/house2.jpeg",
  ];

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      body: new SingleChildScrollView(
        child: new Column(
          children: <Widget>[
            new Stack(
              children: <Widget>[
                new Container(
                  width: double.infinity,
                  height: 300.0,
                  child: new Stack(
                    children: <Widget>[
                      new Container(
                        width: double.infinity,
                        height: 300.0,
                        child: PageView(
                          controller: _pageController,
                          onPageChanged: (int index){
                            setState(() {

                              currentPosition  = index + 1;
                            });
                          },
                          children: photos.map((photo){
                            return new Container(
                              width: double.infinity,
                              height: 300.0,
                              child: InkWell(
                                onTap: (){
                                  Navigator.push(context, new MaterialPageRoute(builder: (context)=>new FullScreenImageWidget(photos)));
                                },
                                child: Image.asset(

                                  photo,
                                  width: MediaQuery.of(context).size.width,
                                  height: 300.0,

                                  fit: BoxFit.cover,) /*new FadeInImage(
                                  image: new NetworkImage(photo.image),
                                  fit: BoxFit.cover,
                                  placeholder: new AssetImage("images/placeholder.png"),
                                )*/,
                              ),
                            );
                          }).toList(),
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.all(10.0),
                        height: 300,
                        color: Colors.transparent,
                        alignment: Alignment.bottomCenter,
                        child: PageViewIndicator(
                          pageIndexNotifier: pageIndexNotifier,
                          length: photos.length,
                          normalBuilder: (animationController, index) => Circle(
                            size: 9.0,
                            color: Colors.grey[200],
                          ),
                          highlightedBuilder: (animationController, index) => ScaleTransition(
                            scale: CurvedAnimation(
                              parent: animationController,
                              curve: Curves.ease,
                            ),
                            child: Circle(
                              size: 11.0,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                new AppBar(
                  leading: IconButton(icon: Icon(Icons.arrow_back, color: Colors.white,), onPressed: (){
                    Navigator.pop(context);
                  }),
                  backgroundColor: Colors.transparent,
                  centerTitle: false,
                  title: new Text('', style: new TextStyle(color: Colors.white, fontFamily: 'Montserrat', fontSize: 18.0, fontWeight: FontWeight.bold),),
                  actions: <Widget>[

                    IconButton(icon: Icon(Icons.share, color: Colors.white,), onPressed: (){

                    }),
                    IconButton(icon: Icon(Icons.favorite_border, color: Colors.white,), onPressed: (){

                    }),

                  ],
                )
              ],
            ),
            new Container(
              padding: EdgeInsets.only(left: 20.0, right: 20.0),
              child: new Column(
                children: <Widget>[
                  new SizedBox(height:20.0),
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Text('RESIDENCES MEUBLEES', style: new TextStyle(color: Colors.blue[900], fontWeight: FontWeight.bold),),
                  ),
                  new SizedBox(height:10.0),
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Text('RESIDENCES ABBA A Angré', style: new TextStyle(color: Colors.black87, fontSize: 30.0,fontWeight: FontWeight.bold),),
                  ),

                  new SizedBox(height:20.0),
                  new Container(
                    alignment: Alignment.topLeft,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Container(
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text('Abidjan, Côte d\'Ivoire', style: new TextStyle(color: Colors.black87, fontSize: 15.0,),),
                              new Text('Hôte: Fabrice Koffi', style: new TextStyle(color: Colors.black87, fontSize: 15.0,),),
                            ],
                          ),
                        ),
                        new Container(
                          width: 80.0,
                          height: 80.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage('assets/images/girl.jpg'))
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  new Container(
                    child: new Row(
                     children: <Widget>[
                       new Icon(Icons.home),
                       SizedBox(width: 10.0,),
                       new Text('Appartement en résidence entier', style: new TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16.0),)
                     ],
                    ),
                  ),
                SizedBox(height: 5.0,),
                new Container(
                  child: new Row(
                    children: <Widget>[
                      SizedBox(width: 36.0,),
                      new Expanded(
                        child: new Text('2 Voyageurs - 1 Chambre - 1 lit - 1 salle de bain', style: new TextStyle(color: Colors.black, fontSize: 16.0),),
                      )
                    ],
                  ),
                ),

                SizedBox(height: 10.0,),
                Divider(),
                SizedBox(height: 10.0,),
                new Container(
                  width: MediaQuery.of(context).size.width - 40.0,
                  child: new Text('There is tension between centralism and decentralization and it is a political dynamic as old as civilization. The decentralized dream is partly about society going to the next level where layers of the value draining middle-men we suffer now are gone.', style: new TextStyle(color: Colors.black, fontSize: 16.0),),
                ),
                SizedBox(height: 10.0,),
                Divider(),
                SizedBox(height: 10.0,),
                new Container(
                      alignment: Alignment.topLeft,
                      child: Text("Couchages",style: TextStyle(fontSize: 25.0,color: Colors.black,fontWeight: FontWeight.bold),)
                ),
                  SizedBox(height: 10.0,),
                  new Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black26),
                      borderRadius: BorderRadius.circular(5.0)
                    ),
                    child: new Column(
                      children: <Widget>[
                        new Container(
                            alignment: Alignment.topLeft,
                            child: Text("Chambre 1",style: TextStyle(fontSize: 18.0,color: Colors.black,fontWeight: FontWeight.bold),)
                        ),
                        new Container(
                            alignment: Alignment.topLeft,
                            child: Text("1 lit queen size, 1 canapé",style: TextStyle(fontSize: 16.0,color: Colors.black,),)
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10.0,),
                  new Container(
                      alignment: Alignment.topLeft,
                      child: Text("Equipements",style: TextStyle(fontSize: 25.0,color: Colors.black,fontWeight: FontWeight.bold),)
                  ),
                  new Container(
                    height: 300.0,
                    width: MediaQuery.of(context).size.width - 20.0,
                    child: GridView.count(
                      crossAxisCount: 2,
                      children: <Widget>[
                        new Container(
                          height: 40.0,
                          alignment: Alignment.topLeft,
                          child: new Text('* Climatisation', style: new TextStyle(color: Colors.black, fontSize: 16.0),),
                        ),

                        new Container(
                          height: 40.0,
                          alignment: Alignment.topLeft,
                          child: new Text('* Sèche-linge', style: new TextStyle(color: Colors.black, fontSize: 16.0),),
                        ),
                      ],
                    ),
                  )



                ],
              ),
            ),

          ],
        ),
      ),
    );

  }

}
